import numpy as np
import warnings
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from prettytable import PrettyTable


df = pd.read_csv("/media/arima/DATA/Arima/PROJECTS/Outbox/YetToBePaid/Red Wine Project/Datasets/DS1/winequality-red.csv")

print(df.head(10))

print(df.shape)

df.columns = df.columns.str.replace(' ', '_')

df.info()
df.isnull().sum()

#let's start visualizing the different quality values and how many wines have that rating in our dataset
sns.countplot(df['quality'])
df['quality'].value_counts()

#Calculate and order correlations
correlations = df.corr()['quality'].sort_values(ascending=False)
print(correlations)

correlations.plot(kind='bar')
plt.savefig('/media/arima/DATA/Arima/PROJECTS/Outbox/YetToBePaid/Red Wine Project/Datasets/DS1/Correlations')
plt.show()

#Heatmap to plot all correlations between features
plt.figure(figsize=(10,6))
sns.heatmap(df.corr(), annot=True, fmt='.0%')
plt.savefig('/media/arima/DATA/Arima/PROJECTS/Outbox/YetToBePaid/Red Wine Project/Datasets/DS1/Heatmap_of_Correlations_between_features')
plt.show()

print(abs(correlations) > 0.2)

#Alcohol Percent
bp = sns.boxplot(x='quality',y='alcohol', data=df)
bp.set(title="Alcohol Percent in Different Quality Wines")
plt.savefig('/media/arima/DATA/Arima/PROJECTS/Outbox/YetToBePaid/Red Wine Project/Datasets/DS1/Alcohol_Percentage')
plt.show()
df_quality_five_six = df.loc[(df['quality'] >= 5) & (df['quality'] <= 6)]
df_quality_five_six['quality'].value_counts()

correlations_subset = df_quality_five_six.corr()['quality'].sort_values(ascending=False)
print(correlations_subset)

#Sulphates and Citric Acid Presence
bp = sns.boxplot(x='quality',y='sulphates', data=df)
bp.set(title="Sulphates in Different Quality Wines")
plt.savefig('/media/arima/DATA/Arima/PROJECTS/Outbox/YetToBePaid/Red Wine Project/Datasets/DS1/Sulphates_in_Different_Quality_Wines')
plt.show()
bp = sns.boxplot(x='quality',y='citric_acid', data=df)
bp.set(title="Citric Acid in Different Quality Wines")
plt.savefig('/media/arima/DATA/Arima/PROJECTS/Outbox/YetToBePaid/Red Wine Project/Datasets/DS1/Citric_Acid_in_Different_Quality_Wines')
plt.show()
#Acetic Acid Presence
bp = sns.boxplot(x='quality',y='volatile_acidity', data=df)
bp.set(title="Acetic Acid Presence in Different Quality Wines")
plt.savefig('/media/arima/DATA/Arima/PROJECTS/Outbox/YetToBePaid/Red Wine Project/Datasets/DS1/Acetic_Acid_in_Different_Quality_Wines')
plt.show()
#We make a copy of our dataframe and group quality in differnt groups
df_aux = df.copy()
df_aux['quality'].replace([3,4],['low','low'],inplace=True)
df_aux['quality'].replace([5,6],['med','med'],inplace=True)
df_aux['quality'].replace([7,8],['high','high'],inplace=True)

sns.countplot(df_aux['quality'])
plt.savefig('/media/arima/DATA/Arima/PROJECTS/Outbox/YetToBePaid/Red Wine Project/Datasets/DS1/Quality')
plt.show()
#We pot some histograms that show the values of features selected
flistt = ['alcohol','sulphates','citric_acid','volatile_acidity']
low = df_aux[df_aux['quality'] == 'low']
medium = df_aux[df_aux['quality'] == 'med']
high = df_aux[df_aux['quality'] == 'high']
plt.rcParams.update({'font.size': 8})
plot, graphs = plt.subplots(nrows= 2, ncols= 2, figsize=(12,6))
graphs = graphs.flatten()
for i, graph in enumerate(graphs):
    graph.figure
    binwidth= (max(df_aux[flistt[i]]) - min(df_aux[flistt[i]]))/30
    bins = np.arange(min(df[flistt[i]]), max(df_aux[flistt[i]]) + binwidth, binwidth)
    graph.hist([low[flistt[i]],medium[flistt[i]],high[flistt[i]]], bins=bins, alpha=0.6, density=True, label=['Low','Medium','High'], color=['red','green','blue'])
    graph.legend(loc='upper right')
    graph.set_title(flistt[i])
plt.tight_layout()
plt.savefig('/media/arima/DATA/Arima/PROJECTS/Outbox/YetToBePaid/Red Wine Project/Datasets/DS1/All_Graphs')
plt.show()


